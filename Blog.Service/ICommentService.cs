﻿using Blog.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Service
{
    public interface ICommentService
    {
        List<Comment> GetCommentList(Int64 idArticle);
        Comment GetComment(Int64 Id);
        void InsertComment(Comment comment);
        void UpdateComment(Comment comment);
        void DeleteComment(Comment comment);
        
    }
}
