﻿using Blog.Data;
using Blog.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blog.Service
{
    public class ArticleService:IArticleService
    {
        private IRepository<Article> _articleRepository;

        public ArticleService(IRepository<Article> articleRepository)
        {
            this._articleRepository = articleRepository;
        }

        public IEnumerable<Article> GetArticle()
        {
            return _articleRepository.GetAll();
        }

        public IEnumerable<Article> SearchArticle(string keyword)
        {
            return _articleRepository.GetAll().Where(m=>m.Title.Contains(keyword));
        }


        public Article GetArticle(Int64 id)
        {
            return _articleRepository.Entity.Include(m => m.Comment).FirstOrDefault(m => m.Id == id);
        }

       
        public void InsertArticle(Article article)
        {
            _articleRepository.Insert(article);
        }
        public void UpdateArticle(Article article)
        {
            _articleRepository.Update(article);
        }
        public void DeleteArticle(Article article)
        {
            _articleRepository.Delete(article);
        }
    }
}
