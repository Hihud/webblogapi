﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.Data;
using Blog.Repository;
using System.Linq;

namespace Blog.Service
{
    public class CommentService : ICommentService
    {
        private readonly IRepository<Comment> _commentRepository;

        public CommentService(IRepository<Comment> commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public void DeleteComment(Comment comment)
        {
            _commentRepository.Delete(comment);
        }

        public List<Comment> GetCommentList(Int64 idArticle)
        {
            return _commentRepository.GetAll().Where(m => m.ArticleId == idArticle).OrderByDescending(d => d.DateCreate).ToList();
        }
        public Comment GetComment(Int64 Id)
        {
            return _commentRepository.GetById(Id);
        }

        public void InsertComment(Comment comment)
        {
            _commentRepository.Insert(comment);
        }

        public void UpdateComment(Comment comment)
        {
            _commentRepository.Update(comment);
        }
    }
}
