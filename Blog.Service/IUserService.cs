﻿using Blog.Data;
using Blog.Data.ApiModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Service
{
    public interface IUserService
    {
        IEnumerable<User> GetUser();
        User SignIn(string Username, string Password);
        string Validate(string Username, string Email);
        User GetUser(long Id);
        User GetUser(string Username);
        void InsertUser(User User);
        void UpdateUser(User User);
        Int64 GetUserId(string Username);
        string HashCode(string Username, string Password);
        UserModel AuthenticateUser(LoginModel model);
        string GenerateJSONWebToken(UserModel user);
        UserModel DecodeJwtToken(string token);
    }
}
