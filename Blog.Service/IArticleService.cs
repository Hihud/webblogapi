﻿using Blog.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Service
{
    public interface IArticleService
    {
        IEnumerable<Article> GetArticle();
        IEnumerable<Article> SearchArticle(string keyword);
        Article GetArticle(long id);
        void InsertArticle(Article article);
        void UpdateArticle(Article article);
        void DeleteArticle(Article article);
        //List<Article> GetRecentArticle();


    }
}
