﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.Data;
using Blog.Repository;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using Blog.Data.ApiModel;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;

namespace Blog.Service
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly ApplicationContext _appContext;
        private IConfiguration _config;
        public UserService(IRepository<User> userRepository, ApplicationContext appContext, IConfiguration config)
        {
            _userRepository = userRepository;
            _appContext = appContext;
            _config = config;
        }
        public IEnumerable<User> GetUser()
        {
            return _userRepository.GetAll();
        }

        public User GetUser(long id)
        {
            return _userRepository.GetById(id);
        }

        public User GetUser(string Username)
        {
            return _userRepository.GetAll().FirstOrDefault(m => m.UserName == Username);

        }

        public void InsertUser(User user)
        {
            _userRepository.Insert(user);
        }

        public void UpdateUser(User user)
        {
            _userRepository.Update(user);
        }
               
        public User SignIn(string Username, string Password)
        {

            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                return null;
            var user = _userRepository.GetAll().FirstOrDefault(m => m.UserName == Username && m.HashCode == Password);
            if (user == null)
                return null;
            return user;

        }

        public string Validate(string Username, string Email)
        {
            if (_userRepository.GetAll().FirstOrDefault(m => m.UserName == Username) != null)
                return("Username "+ Username +" has been taken!");
            if (_userRepository.GetAll().FirstOrDefault(m => m.Email == Email) != null)
               return("E-mail " + Email + " has been taken!");
            return null;
        }

        public Int64 GetUserId(string UserName)
        {
            var user = _userRepository.GetAll().FirstOrDefault(m => m.UserName == UserName);
            return user.Id;
        }

        public string HashCode(string username, string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            var hashuser = md5.ComputeHash(Encoding.ASCII.GetBytes(username));
            byte[] resultusername = md5.Hash;

            byte[] result = resultusername;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }
            var hashpass = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));
            byte[] resultpassword = md5.Hash;
            result = resultpassword;
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }
            strBuilder.Replace("a", "ba3");
            return strBuilder.ToString();
        }

        public UserModel AuthenticateUser(LoginModel model)
        {
            var user = SignIn(model.Username, HashCode(model.Username, model.Password));
            if (user == null)
                return null;
            return new UserModel
            {
                Id = user.Id,
                Username = user.UserName,
                FullName = user.FullName,
                Email = user.Email
            };
        }

        public string GenerateJSONWebToken(UserModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[] {
                new Claim(ClaimTypes.Name, user.Id.ToString()),
                new Claim("Username", user.Username),
                new Claim("Email", user.Email),
                new Claim("FullName", user.FullName)
            };
            var key = Encoding.ASCII.GetBytes(_config["Jwt:Key"]);
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddDays(1),
              signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public UserModel DecodeJwtToken(string token)
        {
            UserModel user = new UserModel();
            string secret = _config["Jwt:Key"];
            var key = Encoding.ASCII.GetBytes(secret);
            var handler = new JwtSecurityTokenHandler();
            var tokenSecure = handler.ReadToken(token) as SecurityToken;
            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };
            
            var claims = handler.ValidateToken(token, validations, out tokenSecure);
            
            foreach (Claim claim in claims.Claims)
            {
                switch (claim.Type)
                {
                    case "Username":
                        user.Username = claim.Value;
                        break;
                    case "Email":
                        user.Email = claim.Value;
                        break;
                    case "FullName":
                        user.FullName = claim.Value;
                        break;
                    default:
                        break;
                }
            }
            user.Id = Convert.ToInt64(claims.Identity.Name);
            return user;
        }
    }
}
