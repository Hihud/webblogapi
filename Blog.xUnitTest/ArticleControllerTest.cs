using Blog.Data;
using BlogApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Blog.Service;
using FluentAssertions;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Blog.Data.ApiModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Blog.xUnitTest
{
    public class ArticleControllerUnitTests
    {
        [Theory]
        [InlineData(-1, -1)]
        [InlineData(-1, 0)]
        [InlineData(0, 0)]
        [InlineData(0, -1)]
        public void GetArticleTest_WithInvalidParameters_ShouldReturnBadRequest(int currentPage, int articlePerPage)
        {
            //Arrange
            var listArticle = InitArticleList();
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle()).Returns(() => listArticle);
            var userServiceMock = new Mock<IUserService>();
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var actionResult = controller.GetArticle(currentPage, articlePerPage);
            //Assert
            Assert.IsType<BadRequestObjectResult>(actionResult);

        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(1, 2)]
        [InlineData(10, 3)]
        public void GetArticleTest_WithValidParameters_ShouldReturnOkObject(int currentPage, int articlePerPage)
        {
            //Arrange
            var listArticle = InitArticleList();
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle()).Returns(() => listArticle);
            var userServiceMock = new Mock<IUserService>();
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var actionResult = controller.GetArticle(currentPage, articlePerPage);
            //Assert
            Assert.IsType<OkObjectResult>(actionResult);
        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(1, 2)]
        [InlineData(1, 3)]
        public void GetArticleTest_WithValidParameters_ShouldReturnArticleCountPerPage(int currentPage, int articlePerPage)
        {
            //Arrange
            var listArticle = InitArticleList();
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle()).Returns(() => listArticle);
            var userServiceMock = new Mock<IUserService>();
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var actionResult = controller.GetArticle(currentPage, articlePerPage);
            var okResult = (OkObjectResult)actionResult;
            var result = (GetAllArticleType)okResult.Value;
            int expectArticleCount = listArticle.Skip((currentPage - 1) * articlePerPage).Take(articlePerPage).Count();
            //Assert
            Assert.Equal(expectArticleCount, result.ListArticle.Count());
        }


        [Theory]
        [InlineData(1, 1)]
        [InlineData(1, 2)]
        [InlineData(1, 3)]
        public void GetArticleTest_WithValidParameters_ShouldReturnTotalArticleCount(int currentPage, int articlePerPage)
        {
            //Arrange
            var listArticle = InitArticleList();
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle()).Returns(() => listArticle);
            var userServiceMock = new Mock<IUserService>();
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var actionResult = controller.GetArticle(currentPage, articlePerPage);
            var okResult = (OkObjectResult)actionResult;
            var result = (GetAllArticleType)okResult.Value;
            int expectArticleCount = listArticle.Count;
            //Assert
            Assert.Equal(expectArticleCount, result.TotalArticle);
        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(1, 2)]
        [InlineData(1, 3)]
        public void GetArticleTest_WithValidParameters_ShouldReturnArticleList(int currentPage, int articlePerPage)
        {
            //Arrange
            var listArticle = InitArticleList();
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle()).Returns(() => listArticle);
            var userServiceMock = new Mock<IUserService>();
            var controller = new ArticleController(articleServiceMock.Object,  userServiceMock.Object);
            //Act
            var actionResult = controller.GetArticle(currentPage, articlePerPage);
            var okResult = (OkObjectResult)actionResult;
            var result = (GetAllArticleType)okResult.Value;
            var expectArticle = listArticle.Skip((currentPage - 1) * articlePerPage).Take(articlePerPage);
            //Assert
            Assert.Equal(expectArticle, result.ListArticle);
        }

        [Theory]
        [InlineData(-18)]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(6)]
        public void GetArticleByIdTest_WithInvalidParameter_ShouldReturnBadRequest(int InvalidId)
        {
            //Arrange
            var listArticle = InitArticleList();
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle(InvalidId)).Returns(() => listArticle.FirstOrDefault(m => m.Id == InvalidId));
            var userServiceMock = new Mock<IUserService>();
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var actionResult = controller.GetArticle(InvalidId);
            //Assert
            Assert.IsType<BadRequestObjectResult>(actionResult);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GetArticleByIdTest_WithValidParameter_ShouldReturnOkObject(int ValidId)
        {
            //Arrange
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle(ValidId)).Returns(new Article());
            var userServiceMock = new Mock<IUserService>();
            var controller = new ArticleController(articleServiceMock.Object,  userServiceMock.Object);

            //Act
            var actionResult = controller.GetArticle(ValidId);

            //Assert
            Assert.IsType<OkObjectResult>(actionResult);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GetArticleByIdTest_WithValidParameter_ShouldReturnAnArticle(int ValidId)
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            articleServiceMock.Setup(x => x.GetArticle(ValidId)).Returns(() => listArticle.First(m => m.Id == ValidId));
            var userServiceMock = new Mock<IUserService>();
            var UserId = listArticle.FirstOrDefault(m => m.Id == ValidId).Comment.Select(m => m.UserId);
            foreach (var Id in UserId)
                userServiceMock.Setup(x => x.GetUser(Id)).Returns(() => listUser.FirstOrDefault(m => m.Id == Id));
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var actionResult = controller.GetArticle(ValidId);
            var okResult = (OkObjectResult)actionResult;
            var result = (Article)okResult.Value;
            var expectArticle = listArticle.First(m => m.Id == ValidId);
            //Assert
            result.Should().BeEquivalentTo(expectArticle);
        }


        [Fact]
        public void CreateArticleTest_WithInvalidModelState_ShouldReturnBadRequest()
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var articlemodel = new ArticleModel {};
            var controller = new ArticleController(articleServiceMock.Object,userServiceMock.Object);
            controller.ModelState.AddModelError("UnitTest", "InvalidModel");
            //Act
            var result = controller.CreateArticle(articlemodel);
            //Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }


        [Fact]
        public void CreateArticleTest_WithWrongUserId_ShouldReturnBadRequest()
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var InvalidToken = "InvalidToken";
            userServiceMock.Setup(x => x.DecodeJwtToken(InvalidToken)).Returns(() => new UserModel { });
            var articlemodel = new ArticleModel {
                Title = "Title 1",
                Content = "Content 1",
                DateCreate = new DateTime(2018, 10, 10, 11, 30, 00),
                UserId = 1,
                Username = "Phan Huynh Duc",
            };
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["Authorization"] = "Bearer " + InvalidToken;
            //Act
            var result = controller.CreateArticle(articlemodel);
            //Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public void CreateArticleTest_WithValidTokenAndValidModel_ShouldReturnOkObject()
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var InvalidToken = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9";
           
            var articlemodel = new ArticleModel
            {
                Title = "Title 1",
                Content = "Content 1",
                UserId = 1,
                Username = "Phan Huynh Duc",
            };
            userServiceMock.Setup(x => x.DecodeJwtToken(InvalidToken)).Returns(() => new UserModel { Id = articlemodel.UserId});
            articleServiceMock.Setup(x => x.InsertArticle(It.IsAny<Article>()));
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["Authorization"] = "Bearer " + InvalidToken;
            //Act
            var result = controller.CreateArticle(articlemodel);
            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public void UpdateArticleTest_WithInvalidModelState_ShouldReturnBadRequest()
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var articlemodel = new ArticleModel { };
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            controller.ModelState.AddModelError("UnitTest", "InvalidModel");
            //Act
            var result = controller.EditArticle(articlemodel);
            //Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public void UpdateArticleTest_WithInvalidArticle_ShouldReturnNotFound()
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
           
            var articlemodel = new ArticleModel
            {
                Id = 100,
                Title = "Title 1",
                Content = "Content 1",
                DateCreate = new DateTime(2018, 10, 10, 11, 30, 00),
                UserId = 1,
                Username = "Phan Huynh Duc",
            };
            articleServiceMock.Setup(x => x.GetArticle(articlemodel.Id)).Returns(() => listArticle.FirstOrDefault(m => m.Id == articlemodel.Id));
           var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var result = controller.EditArticle(articlemodel);
            //Assert
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public void UpdateArticleTest_WithWrongUserId_ShouldReturnBadRequest()
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var InvalidToken = "InvalidToken";
            userServiceMock.Setup(x => x.DecodeJwtToken(InvalidToken)).Returns(() => new UserModel { });

            var articlemodel = new ArticleModel
            {
                Id = 1,
                Title = "Title 1",
                Content = "Content 1",
                DateCreate = new DateTime(2018, 10, 12, 11, 30, 00),
                UserId = 1,
                Username = "Phan Huynh Duc",
            };
            articleServiceMock.Setup(x => x.GetArticle(articlemodel.Id)).Returns(() => listArticle.FirstOrDefault(m => m.Id == articlemodel.Id));
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["Authorization"] = "Bearer " + InvalidToken;
            //Act
            var result = controller.EditArticle(articlemodel);
            //Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }

        [Fact]
        public void UpdateArticleTest_WithValidTokenAndValidModel_ShouldReturnOkResult()
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var Token = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9";

            var articlemodel = new ArticleModel
            {
                Id = 1,
                Title = "Title 1",
                Content = "Content 1",
                UserId = 1,
                Username = "Phan Huynh Duc",
            };
            userServiceMock.Setup(x => x.DecodeJwtToken(Token)).Returns(() => new UserModel { Id = articlemodel.UserId });
            articleServiceMock.Setup(x => x.GetArticle(articlemodel.Id)).Returns(() => listArticle.FirstOrDefault(m => m.Id == articlemodel.Id));
            articleServiceMock.Setup(x => x.UpdateArticle(It.IsAny<Article>()));
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["Authorization"] = "Bearer " + Token;
            //Act
            var result = controller.EditArticle(articlemodel);
            //Assert
            Assert.IsType<OkResult>(result.Result);
        }

        [Theory]
        [InlineData(15)]
        [InlineData(-5)]
        public void DeleteArticleTest_WithInvalidArticle_ShouldReturnNotFound(int Id)
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();

          
            articleServiceMock.Setup(x => x.GetArticle(Id)).Returns(() => listArticle.FirstOrDefault(m => m.Id == Id));
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            //Act
            var result = controller.Delete(Id);
            //Assert
            Assert.IsType<NotFoundResult>(result);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void DeleteArticleTest_WithWrongUserId_ShouldReturnBadRequest(int Id)
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var InvalidToken = "InvalidToken";
            userServiceMock.Setup(x => x.DecodeJwtToken(InvalidToken)).Returns(() => new UserModel { });

         
            articleServiceMock.Setup(x => x.GetArticle(Id)).Returns(() => listArticle.FirstOrDefault(m => m.Id == Id));
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["Authorization"] = "Bearer " + InvalidToken;
            //Act
            var result = controller.Delete(Id);
            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void DeleteArticleTest_WithValidTokenAndValidArticle_ShouldReturnOkResult(int Id)
        {
            //Arrange
            var listArticle = InitArticleList();
            var listUser = InitialUserList();
            var articleServiceMock = new Mock<IArticleService>();
            var userServiceMock = new Mock<IUserService>();
            var InvalidToken = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9";

            
            articleServiceMock.Setup(x => x.GetArticle(Id)).Returns(() => listArticle.FirstOrDefault(m => m.Id == Id));
            userServiceMock.Setup(x => x.DecodeJwtToken(InvalidToken)).Returns(() => new UserModel {Id = listArticle.FirstOrDefault(m => m.Id == Id).UserId });
            articleServiceMock.Setup(x => x.UpdateArticle(It.IsAny<Article>()));
            var controller = new ArticleController(articleServiceMock.Object, userServiceMock.Object);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["Authorization"] = "Bearer " + InvalidToken;
            //Act
            var result = controller.Delete(Id);
            //Assert
            Assert.IsType<OkResult>(result);
        }


        List<Article> InitArticleList()
        {
            return new List<Article>
              {
                new Article {
                    Id = 1,
                    Title = "Title 1",
                    Content = "Content 1",
                    DateCreate = new DateTime(2018, 10, 10, 11, 30, 00),
                    UserId = 1,
                    Username = "Phan Huynh Duc",
                    Thumbnail = "http://localhost:59065/Upload/79.jpg",
                    Comment = new List<Comment>
                    {
                        new Comment
                        {
                            Id = 1,
                            ArticleId = 1,
                            Content = "Comment 1",
                            DateCreate = new DateTime(2018,10,11),
                            UserId = 1,
                            Username="Phan Huynh Duc"                                
                        },
                        new Comment
                        {
                            Id = 20,
                            ArticleId = 1,
                            Content = "Comment 20",
                            DateCreate = new DateTime(2018,10,11),
                            UserId = 56,
                            Username="Oanh Nguyen"
                        }
                    }
                },
                new Article {
                    Id = 2,
                    Title = "Title 2",
                    Content = "Content 2",
                    DateCreate = new DateTime(2018, 10, 10, 11, 30, 00),
                    UserId = 1,
                    Username = "Phan Huynh Duc",
                    Thumbnail = "http://localhost:59065/Upload/79.jpg",
                    Comment = new List<Comment>
                    {}
                },
                new Article {
                    Id = 3,
                    Title = "Title 3",
                    Content = "Content 3",
                    DateCreate = new DateTime(2018, 10, 10, 11, 30, 00),
                    UserId = 1,
                    Username = "Phan Huynh Duc",
                    Thumbnail = "http://localhost:59065/Upload/79.jpg",
                     Comment = new List<Comment>
                    {}
                },
             };
        }

        List<User> InitialUserList()
        {
            return new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "bbhud",
                    Email = "abc@def",
                    FullName = "Phan Huynh Duc",
                    HashCode= "7ff1644540f1b8c346f755b3f3d9d7d137cba3925ba34775cba3091b2f1478bb31f99d"
                },
                new User
                {

                    Id = 56,
                    UserName = "oanh",
                    Email = "oanh@2k",
                    FullName = "Oanh Nguyen",
                    HashCode= "55806840cfd69ce594ba3ba318e621bf805ba3e10ba3dc3949bba359ba3bbe56e057f20f883e"
                }
            };
        }

      
    }
    
}
