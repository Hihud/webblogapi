﻿using System;

namespace BlogApi.Controllers
{
    public class GetArticleType
    {
        public Int64 Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Int64 UserId { get; set; }
        public string Username { get; set; }
        public string Thumbnail { get; set; }
        public DateTime DateCreate { get; set; }
        public System.Linq.IOrderedEnumerable<object> Comment { get; set; }
    }
}