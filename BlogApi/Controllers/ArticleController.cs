﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Blog.Service;
using Blog.Repository;
using Microsoft.AspNetCore.Authorization;
using Blog.Data;
using Blog.Data.ApiModel;
using Microsoft.EntityFrameworkCore;
using System.IO;

namespace BlogApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Article")]
    [Authorize]
    public class ArticleController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly IUserService _userService;
        private readonly ICommentService _commentService;
        public ArticleController(IArticleService articleService, IUserService userService)
        {
            this._articleService = articleService;
            this._userService = userService;
        }

        [HttpGet]
        [Route("GetArticle")]
        [AllowAnonymous]
        public IActionResult GetArticle(int page, int count)
        {
            if(page<=0||count<=0)
            {
                return BadRequest("Invalid parameter");
            }
            int totalarticle = _articleService.GetArticle().Count();
            var listArticle = _articleService.GetArticle().OrderByDescending(m => m.DateCreate).Skip((page - 1) * count).Take(count);
            return Ok(new GetAllArticleType
            {
                ListArticle = listArticle,
                TotalArticle= totalarticle
            });
        }   

        [HttpGet]
        [Route("GetArticle/{id}")]
        [AllowAnonymous]
        public IActionResult GetArticle(Int64 Id)
        {
            if(Id<=0)
            {
                return BadRequest("Invalid Parameter");
            }
            var article = _articleService.GetArticle(Id);
            if (article != null)
                return Ok(new Article
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    UserId = article.UserId,
                    Username = article.Username,
                    Thumbnail = article.Thumbnail,
                    DateCreate = article.DateCreate,
                    Comment = article.Comment?.OrderByDescending(m => m.DateCreate).Select(m => new Comment
                    {
                        Id = m.Id,
                        UserId = m.UserId,
                        Content = m.Content,
                        ArticleId = m.ArticleId,
                        DateCreate = m.DateCreate,
                        Username = _userService.GetUser(m.UserId).FullName
                    }).ToList()
                });
            else return BadRequest("Null");
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> CreateArticle(ArticleModel model)
        {
            if (ModelState.IsValid)
            {
                var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
                if (_userService.DecodeJwtToken(token).Id != model.UserId)
                {
                    return BadRequest("You cannot upload article with another user's name!");
                }
                var tempThumnail = Guid.NewGuid();
                Article article = new Article
                {
                    Title = model.Title,
                    Content = model.Content,
                    UserId = model.UserId,
                    Username = model.Username,
                    DateCreate = DateTime.Now.ToUniversalTime()                   
                 };
                if (model.image != null)
                {
                    article.Thumbnail = "http://localhost:59065/Upload/" + tempThumnail + Path.GetExtension(model.image.FileName);
                }
                _articleService.InsertArticle(article);
               
                if (model.image!=null)
                {
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", $"{tempThumnail}{Path.GetExtension(model.image.FileName)}");
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await model.image.CopyToAsync(stream);
                    }
                }
                return Ok(new { article.Id });
            }
            return BadRequest("Input is Invalid");
        }
        [HttpPut]
        [Route("Edit/{id}")]
        public async Task<IActionResult> EditArticle(ArticleModel model)
        {
            if (ModelState.IsValid)
            {
                
                Article article = _articleService.GetArticle(model.Id);
                if (article == null)
                {
                    return NotFound();
                }
                var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
                if (_userService.DecodeJwtToken(token).Id != article.UserId)
                {
                    return BadRequest("You cannot edit another user's article !");
                }
                try
                {
                    article.Title = model.Title;
                    article.Content = model.Content;
                    if (model.image!=null)
                    {
                        var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Upload", $"{article.Id}{Path.GetExtension(model.image.FileName)}");
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await model.image.CopyToAsync(stream);
                        }
                        article.Thumbnail = "http://localhost:59065/Upload/" + article.Id + Path.GetExtension(model.image.FileName);
                    }
                    _articleService.UpdateArticle(article);
                }
                catch (DbUpdateConcurrencyException)
                {
                        throw;
                }
                return Ok();
            }
            return BadRequest("Invalid Model");
        }
        [HttpDelete]
        [Route("Delete/{id}")]
        public IActionResult Delete(Int64 Id)
        {
            Article article = _articleService.GetArticle(Id);
            if(article==null)
            {
                return NotFound();
            }
            var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
            if(_userService.DecodeJwtToken(token).Id!=article.UserId)
            {
                return BadRequest("You cannot delete another user's article!");
            }
            _articleService.DeleteArticle(article);
            return Ok();
        }
    }
}