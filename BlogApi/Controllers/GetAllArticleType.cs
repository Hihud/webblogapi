﻿using Blog.Data;
using System.Collections.Generic;

namespace BlogApi.Controllers
{
   public class GetAllArticleType
    {
        public IEnumerable<Article> ListArticle { get; set; }
        public int TotalArticle { get; set; }
    }
}