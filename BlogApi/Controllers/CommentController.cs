﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Blog.Service;
using Blog.Data.ApiModel;
using Blog.Data;
using Microsoft.EntityFrameworkCore;

namespace BlogApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Comment")]
    [Authorize]
    public class CommentController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;

        public CommentController(IArticleService articleService, ICommentService commentService, IUserService userService)
        {
            _articleService = articleService;
            _commentService = commentService;
            _userService = userService;
        }

        [HttpPost]
        [Route("AddComment")]
        public IActionResult AddComment(CommentModel model)
        {
            var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
            Comment comment = new Comment
            {
                ArticleId = model.ArticleId,
                Content = model.Content,
                DateCreate = DateTime.Now,
                UserId = _userService.DecodeJwtToken(token).Id
            };
            Article article = _articleService.GetArticle(model.ArticleId);
            if (article == null)
                return NotFound();
            article.Comment.Add(comment);

            _commentService.InsertComment(comment);
            _articleService.UpdateArticle(article);
            return Ok(new {
                Id = comment.Id,
                ArticleId = comment.ArticleId,
                Content = comment.Content,
                DateCreate = comment.DateCreate,
                UserId = comment.UserId,
                Username = _userService.GetUser(comment.UserId).FullName
            });
        }
        [HttpPut]
        [Route("EditComment")]
        public IActionResult EditComment(CommentModel model)
        {
            var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
            if (model.UserId != _userService.DecodeJwtToken(token).Id)
                return Unauthorized();
            Comment comment = _commentService.GetComment(model.Id);
            comment.Content = model.Content;
            comment.DateCreate = DateTime.Now;
            Article article = _articleService.GetArticle(model.ArticleId);
            if (article == null)
                return NotFound();
            try
            {
               
                _commentService.UpdateComment(comment);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
           // _commentService.UpdateComment(comment);
            return Ok();
        }
        [HttpDelete]
        [Route("DeleteComment/{Id}")]
        public IActionResult DeleteComment(Int64 Id)
        {
            Comment comment = _commentService.GetComment(Id);
            var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
            if (comment.UserId != _userService.DecodeJwtToken(token).Id)
                return Unauthorized();
            _commentService.DeleteComment(comment);
            return Ok();

        }

    }
}