﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Blog.Service;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using Blog.Data;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Cryptography;
using Blog.Data.ApiModel;

namespace BlogApi.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]

    public class UserController : Controller
    {
        private IConfiguration _config;
        private readonly IUserService _userService;
        private IHttpContextAccessor _httpContextAccessor;
        MD5 md5 = new MD5CryptoServiceProvider();

        public UserController(IConfiguration config, IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            _config = config;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;

        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Register")]
        public IActionResult Register([FromBody]RegisterModel model)
        {
            if(ModelState.IsValid)
            {
                if (_userService.Validate(model.UserName, model.Email) != null)
                {
                    ModelState.AddModelError("Model", _userService.Validate(model.UserName, model.Email).ToString());
                    return BadRequest(ModelState);
                }
                var user = new User
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    FullName = model.FullName,
                    HashCode = _userService.HashCode(model.UserName, model.Password)
                };
                try
                {
                    _userService.InsertUser(user);
                    return Ok();
                }
                catch (ApplicationException ex)
                {
                    return BadRequest(new { message = ex.Message });
                }
            }
            return BadRequest(ModelState);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] LoginModel model)
        {
            IActionResult response = Unauthorized();

            var user = _userService.AuthenticateUser(model);

            if (user != null)
            {
                var tokenString = _userService.GenerateJSONWebToken(user);
                response = Ok(new { token = tokenString, user});
            }
            return response;
        }

        [HttpPut]
        [Route("Update")]
        [Authorize]
        public IActionResult Update([FromBody]UserModel model)
        {
            var accessToken = Request.Headers["Authorization"];
            accessToken = accessToken.ToString().Split(' ')[1];
            UserModel tokenUser = _userService.DecodeJwtToken(accessToken);
            if (tokenUser == null)
                return Unauthorized();

            if (_userService.Validate(model.Username, model.Email)==null)
            {
                var user = _userService.GetUser(tokenUser.Id);
                user.UserName = model.Username;
                user.Email = model.Email;
                user.FullName = model.FullName;
                try
                {
                    _userService.UpdateUser(user);
                    return Ok();
                }
                catch (ApplicationException ex)
                {
                    return BadRequest(new { message = ex.Message });
                }
            }

            return BadRequest();
        }
    }
}