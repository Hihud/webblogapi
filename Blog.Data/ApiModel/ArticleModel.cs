﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.Data.ApiModel
{
    public class ArticleModel:BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreate { get; set; }
        public IFormFile image { get; set; }
        public Int64 UserId { get; set; }
            
    }
}
