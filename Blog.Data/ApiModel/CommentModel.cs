﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.Data.ApiModel
{
    public class CommentModel:BaseEntity
    {
        [Required]
        public Int64 ArticleId { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string DateComment { get; set; }
        [Required]
        public Int64 UserId { get; set; }

        public UserModel User { get; set; }
        public ArticleModel Article { get; set; }
    }
}
