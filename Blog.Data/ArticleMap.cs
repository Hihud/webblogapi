﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class ArticleMap
    {
        public ArticleMap(EntityTypeBuilder<Article> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.Title).IsRequired();
            entityTypeBuilder.Property(t => t.Content).IsRequired();
            entityTypeBuilder.Property(t => t.Username);
            entityTypeBuilder.Property(t => t.DateCreate);
            entityTypeBuilder.HasOne(t => t.User).WithMany(t => t.Article).HasForeignKey(x => x.UserId);
        }
    }
}
