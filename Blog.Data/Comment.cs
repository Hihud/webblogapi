﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.Data
{
    public class Comment:BaseEntity
    {
        [ForeignKey("Article")]
        public Int64 ArticleId { get; set; }

        public string Content { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateCreate { get; set; }
        public string Username { get; set; }
        [ForeignKey("User")]
        public Int64 UserId { get; set; }
        public virtual Article Article { get; set; }
        public virtual User User { get; set; }
    }
}
