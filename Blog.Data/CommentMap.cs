﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class CommentMap
    {
        public CommentMap(EntityTypeBuilder<Comment> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.Content);
            entityTypeBuilder.Property(t => t.UserId);
            entityTypeBuilder.HasOne(t => t.Article).WithMany(t => t.Comment).HasForeignKey(x => x.ArticleId);
            entityTypeBuilder.HasOne(t => t.User).WithMany(t => t.Comment).HasForeignKey(x => x.UserId).OnDelete(Microsoft.EntityFrameworkCore.DeleteBehavior.Restrict);
            entityTypeBuilder.Property(t => t.DateCreate);
        }
    }
}
