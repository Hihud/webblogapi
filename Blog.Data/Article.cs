﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.Data
{
    public class Article:BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }

        [ForeignKey("User")]
        public Int64 UserId { get; set; }
        public string Username { get; set; }
        public string Thumbnail { get; set; }
        public DateTime DateCreate { get; set; }
       
        public virtual User User { get; set; }
        public virtual List<Comment> Comment { get; set; }
    }
}
