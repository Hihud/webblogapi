﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.Data
{
    public class BaseEntity
    {
        [Key]
        public Int64 Id { get; set; }
    }
}
