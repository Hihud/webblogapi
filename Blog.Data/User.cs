﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class User:BaseEntity
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string HashCode { get; set; }
        public virtual List<Article> Article { get; set; }
        public virtual List<Comment> Comment { get; set; }
    }
}
