﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class UserMap
    {
        public UserMap(EntityTypeBuilder<User> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.UserName);
            entityTypeBuilder.Property(t => t.Email);
            entityTypeBuilder.Property(t => t.FullName);
            entityTypeBuilder.Property(t => t.HashCode);
        }
    }
}
